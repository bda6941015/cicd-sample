package com.example.cicdsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CicdSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CicdSampleApplication.class, args);
	}

}
